module.exports = function(grunt) {




    grunt.initConfig({
        exec: {
            buildapp: 'cd js && traceur --out app.js sampleview1.js --modules=instantiate',
            mkbuild: 'mkdir build',
            mvtobuild: 'mv js/app.js build/'

        }
    });

    grunt.loadNpmTasks('grunt-exec');

    grunt.registerTask("buildjs", ["exec:buildapp", "exec:mkbuild", "exec:mvtobuild"]);

    grunt.registerTask('default', ['buildjs']);

};