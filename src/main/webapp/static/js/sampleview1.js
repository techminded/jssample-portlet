import SampleView2 from 'sampleview2';

export default class SampleView1 {
    constructor(params) {
        var sv2 = new SampleView2({ bar: 'baz'});
        console.log('sample view1', sv2, params);
    }
}
var vw = new SampleView1({ foo: 'bar'});